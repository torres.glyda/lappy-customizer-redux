'use strict';

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const runSeeds = () => {
  const collectionName = process.argv[2];
  const location = _path2.default.join(process.cwd(), `src/seeds/${collectionName}.js`);
  const fileExists = _fs2.default.existsSync(location);

  if (fileExists) {
    const seed = require(`./${collectionName}`).default;
    _asyncToGenerator(function* () {
      try {
        yield seed();
        console.log(`${collectionName} seed done.`);
        process.exit(0);
      } catch (error) {
        console.log('ERR: ', error);
      }
    })();
  } else {
    console.log(`${collectionName} file is invalid.`);
  }
};

runSeeds();