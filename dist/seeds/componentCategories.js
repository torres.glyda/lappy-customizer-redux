'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _setupServer = require('../backend/setupServer');

var _setupServer2 = _interopRequireDefault(_setupServer);

var _mongodb = require('mongodb');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const seed = (() => {
  var _ref = _asyncToGenerator(function* () {
    const app = yield (0, _setupServer2.default)();

    const componentCategory = [{
      _id: (0, _mongodb.ObjectId)('59c5031546278b0fccb1ad17'),
      name: 'os'
    }, {
      _id: (0, _mongodb.ObjectId)('59c5031546278b0fccb1ad18'),
      name: 'processor'
    }, {
      _id: (0, _mongodb.ObjectId)('59c5031546278b0fccb1ad19'),
      name: 'accessory'
    }, {
      _id: (0, _mongodb.ObjectId)('59c5031546278b0fccb1ad1a'),
      name: 'bag'
    }];

    const componentCategoryService = yield app.service('api/componentCategories');
    yield componentCategoryService.remove(null);
    return componentCategoryService.create(componentCategory);
  });

  return function seed() {
    return _ref.apply(this, arguments);
  };
})();

exports.default = seed;