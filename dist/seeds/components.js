'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _setupServer = require('../backend/setupServer');

var _setupServer2 = _interopRequireDefault(_setupServer);

var _mongodb = require('mongodb');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const seed = (() => {
  var _ref = _asyncToGenerator(function* () {
    const app = yield (0, _setupServer2.default)();

    const components = [{
      _id: (0, _mongodb.ObjectId)('59c5047aad576a0434e407be'),
      name: 'Ubuntu 14.0.21 LTS (64-bit)',
      categoryId: (0, _mongodb.ObjectId)('59c5031546278b0fccb1ad17'),
      price: 0.00
    }, {
      _id: (0, _mongodb.ObjectId)('59c5047aad576a0434e407bf'),
      name: 'Ubuntu 15.32.2 LTS (64-bit)',
      categoryId: (0, _mongodb.ObjectId)('59c5031546278b0fccb1ad17'),
      price: 0.00
    }, {
      _id: (0, _mongodb.ObjectId)('59c5047aad576a0434e407c0'),
      name: 'Ubuntu 16.04.2 LTS (64-bit)',
      categoryId: (0, _mongodb.ObjectId)('59c5031546278b0fccb1ad17'),
      price: 0.00
    }, {
      _id: (0, _mongodb.ObjectId)('59c5047aad576a0434e407c1'),
      name: 'Ubuntu 17.04 (64-bit)',
      categoryId: (0, _mongodb.ObjectId)('59c5031546278b0fccb1ad17'),
      price: 800.00
    }, {
      _id: (0, _mongodb.ObjectId)('59c5047aad576a0434e407c2'),
      name: 'i3-7100u (3 MB Cache - 2 Cores - 4 Threads)',
      categoryId: (0, _mongodb.ObjectId)('59c5031546278b0fccb1ad18'),
      price: 0.00
    }, {
      _id: (0, _mongodb.ObjectId)('59c5047aad576a0434e407c3'),
      name: 'i7-7500U (2.7 up to 3.5 - 4 MB Cache - 2 Cores - 4 Threads)',
      categoryId: (0, _mongodb.ObjectId)('59c5031546278b0fccb1ad18'),
      price: 3500.00
    }, {
      _id: (0, _mongodb.ObjectId)('59c5047aad576a0434e407c4'),
      name: 'Messenger Bag',
      categoryId: (0, _mongodb.ObjectId)('59c5031546278b0fccb1ad1a'),
      price: 850.00
    }, {
      _id: (0, _mongodb.ObjectId)('59c5047aad576a0434e407c5'),
      name: 'Laptop Backpack',
      categoryId: (0, _mongodb.ObjectId)('59c5031546278b0fccb1ad1a'),
      price: 900.00
    }, {
      _id: (0, _mongodb.ObjectId)('59c5047aad576a0434e407c6'),
      name: 'Extra AC Adapter',
      categoryId: (0, _mongodb.ObjectId)('59c5031546278b0fccb1ad19'),
      price: 450.00
    }, {
      _id: (0, _mongodb.ObjectId)('59c5047aad576a0434e407c7'),
      name: 'Extra Battery',
      categoryId: (0, _mongodb.ObjectId)('59c5031546278b0fccb1ad19'),
      price: 550.00
    }, {
      _id: (0, _mongodb.ObjectId)('59c5047aad576a0434e407c8'),
      name: 'Extrernal USB DVD-RW Drive',
      categoryId: (0, _mongodb.ObjectId)('59c5031546278b0fccb1ad19'),
      price: 1300.00
    }];

    const componentsService = yield app.service('api/components');
    yield componentsService.remove(null);
    return componentsService.create(components);
  });

  return function seed() {
    return _ref.apply(this, arguments);
  };
})();

exports.default = seed;