'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _setupServer = require('../backend/setupServer');

var _setupServer2 = _interopRequireDefault(_setupServer);

var _mongodb = require('mongodb');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const seed = (() => {
  var _ref = _asyncToGenerator(function* () {
    const app = yield (0, _setupServer2.default)();

    const laptops = [{
      _id: (0, _mongodb.ObjectId)('596ba86c6cad6a1aa8277fd5'),
      name: 'Lemur 14"',
      price: 649.00,
      desc: 'Perfect for those on the go, Lemur is easy to carry from meeting to meeting or across campus.',
      componentIds: [(0, _mongodb.ObjectId)('59c5047aad576a0434e407be'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407bf'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407c2'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407c3'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407c4'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407c5'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407c6'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407c7'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407c8')],
      imgSrc: 'images/laptops/lemur-14.jpg'
    }, {
      _id: (0, _mongodb.ObjectId)('596ba86c6cad6a1aa8277fd6'),
      name: 'Gazelle 15"',
      price: 719.00,
      desc: 'Gazelle’s definitive lines sports a high quality, sleek finish. Its points and curves draws the attention of those more bold, making the Gazelle the perfect maker or hacker companion.',
      componentIds: [(0, _mongodb.ObjectId)('59c5047aad576a0434e407bf'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407c0'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407c2'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407c3'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407c4'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407c5'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407c6'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407c7'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407c8')],
      imgSrc: 'images/laptops/gazelle-15.jpg'
    }, {
      _id: (0, _mongodb.ObjectId)('596ba86c6cad6a1aa8277fd7'),
      name: 'Kudu 17"',
      price: 939.00,
      desc: 'Kudu’s high quality finish is sharp and clean, appealing to those who prefer sheer and pure simplicity. Its straight edges and lines keep your machine looking new, and keeps you feeling professional.',
      componentIds: [(0, _mongodb.ObjectId)('59c5047aad576a0434e407c0'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407c1'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407c2'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407c3'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407c4'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407c5'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407c6'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407c7'), (0, _mongodb.ObjectId)('59c5047aad576a0434e407c8')],
      imgSrc: 'images/laptops/kudu-17.jpg'
    }];

    const laptopsService = yield app.service('api/laptops');
    yield laptopsService.remove(null);
    return laptopsService.create(laptops);
  });

  return function seed() {
    return _ref.apply(this, arguments);
  };
})();

exports.default = seed;