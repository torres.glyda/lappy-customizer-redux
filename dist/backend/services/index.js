'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _laptop = require('./laptop');

var _laptop2 = _interopRequireDefault(_laptop);

var _component = require('./component');

var _component2 = _interopRequireDefault(_component);

var _componentCategory = require('./componentCategory');

var _componentCategory2 = _interopRequireDefault(_componentCategory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const services = db => {
  return function () {
    const app = this;
    app.configure((0, _laptop2.default)(db)).configure((0, _component2.default)(db)).configure((0, _componentCategory2.default)(db));
  };
};

exports.default = services;