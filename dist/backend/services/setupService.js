'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _feathersMongodb = require('feathers-mongodb');

var _feathersMongodb2 = _interopRequireDefault(_feathersMongodb);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const setupService = (app, db, collectionName) => {
  const path = collectionName === 'users' ? collectionName : `api/${collectionName}`;
  app.use(path, (0, _feathersMongodb2.default)({ Model: db.collection(collectionName) }));
  const service = app.service(path);
  return service;
};

exports.default = setupService;