'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _setupService = require('./setupService');

var _setupService2 = _interopRequireDefault(_setupService);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const setupComponentCategoryService = db => {
  const before = {};
  const after = {};
  return function () {
    const componentCategoryService = (0, _setupService2.default)(this, db, 'componentCategories');
    componentCategoryService.before(before).after(after);
  };
};

exports.default = setupComponentCategoryService;