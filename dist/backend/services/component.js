'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _setupService = require('./setupService');

var _setupService2 = _interopRequireDefault(_setupService);

var _populate = require('../../hooks/populate');

var _populate2 = _interopRequireDefault(_populate);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const setupComponentService = db => {
  const before = {};
  const after = {
    find: [(0, _populate2.default)('api/componentCategories', '_id', 'categoryId', 'category', true)]
  };
  return function () {
    const componentService = (0, _setupService2.default)(this, db, 'components');
    componentService.before(before).after(after);
  };
};

exports.default = setupComponentService;