'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _feathers = require('feathers');

var _feathers2 = _interopRequireDefault(_feathers);

var _feathersMongodb = require('feathers-mongodb');

var _feathersMongodb2 = _interopRequireDefault(_feathersMongodb);

var _mongodb = require('mongodb');

var _feathersRest = require('feathers-rest');

var _feathersRest2 = _interopRequireDefault(_feathersRest);

var _feathersSocketio = require('feathers-socketio');

var _feathersSocketio2 = _interopRequireDefault(_feathersSocketio);

var _feathersHooks = require('feathers-hooks');

var _feathersHooks2 = _interopRequireDefault(_feathersHooks);

var _feathersConfiguration = require('feathers-configuration');

var _feathersConfiguration2 = _interopRequireDefault(_feathersConfiguration);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _setupReload = require('./setupReload');

var _setupReload2 = _interopRequireDefault(_setupReload);

var _services = require('./services');

var _services2 = _interopRequireDefault(_services);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const app = (0, _feathers2.default)();

(0, _setupReload2.default)(app);

app.configure((0, _feathersRest2.default)()).configure((0, _feathersSocketio2.default)()).configure((0, _feathersHooks2.default)()).configure((0, _feathersConfiguration2.default)(_path2.default.join(process.cwd()))).use(_bodyParser2.default.json()).use(_bodyParser2.default.urlencoded({ extended: true })).use(_feathers2.default.static(_path2.default.join(process.cwd(), 'public')));

const setupServer = (() => {
  var _ref = _asyncToGenerator(function* () {
    const db = yield _mongodb.MongoClient.connect(app.get('mongoURI'));
    app.configure((0, _services2.default)(db));
    return app;
  });

  return function setupServer() {
    return _ref.apply(this, arguments);
  };
})();

exports.default = setupServer;