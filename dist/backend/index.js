'use strict';

var _setupServer = require('./setupServer');

var _setupServer2 = _interopRequireDefault(_setupServer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const port = process.env.PORT || 3000;

const startServer = (() => {
  var _ref = _asyncToGenerator(function* () {
    const app = yield (0, _setupServer2.default)();
    app.listen(port, function () {
      console.log(`App is running at http://localhost:${app.get('port')}`);
    });
  });

  return function startServer() {
    return _ref.apply(this, arguments);
  };
})();

startServer();