'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const setData = (() => {
  var _ref = _asyncToGenerator(function* (specifiedData, service, foreignProp, localProp, nameAs, findOne) {
    const data = specifiedData[localProp];
    const queryItem = data instanceof Array ? { $in: data } : data;
    const foreignData = yield service.find({
      query: {
        [`${foreignProp}`]: queryItem
      }
    });
    specifiedData[nameAs] = findOne ? foreignData[0] : foreignData;
  });

  return function setData(_x, _x2, _x3, _x4, _x5, _x6) {
    return _ref.apply(this, arguments);
  };
})();

const populate = (serviceName, foreignProp, localProp, nameAs, findOne = false) => (() => {
  var _ref2 = _asyncToGenerator(function* (hook) {
    const prop = hook.type === 'after' ? 'result' : 'data';
    let currentData = hook[prop];
    const service = yield hook.app.service(serviceName);
    if (currentData instanceof Array) {
      yield Promise.all(currentData.map((() => {
        var _ref3 = _asyncToGenerator(function* (localData) {
          return yield setData(localData, service, foreignProp, localProp, nameAs, findOne);
        });

        return function (_x8) {
          return _ref3.apply(this, arguments);
        };
      })()));
    } else if (currentData instanceof Object) {
      yield setData(currentData, service, foreignProp, localProp, nameAs, findOne);
    }
    return Promise.resolve(hook);
  });

  return function (_x7) {
    return _ref2.apply(this, arguments);
  };
})();

exports.default = populate;