'use strict';

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _reactRedux = require('react-redux');

var _redux = require('redux');

var _reduxThunk = require('redux-thunk');

var _reduxThunk2 = _interopRequireDefault(_reduxThunk);

var _styledComponents = require('styled-components');

var _theme = require('./styles/theme');

var _theme2 = _interopRequireDefault(_theme);

var _reducers = require('./reducers');

var _reducers2 = _interopRequireDefault(_reducers);

var _AppContainer = require('./containers/AppContainer');

var _AppContainer2 = _interopRequireDefault(_AppContainer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const middleware = [_reduxThunk2.default];
const store = (0, _redux.createStore)(_reducers2.default, (0, _redux.applyMiddleware)(...middleware));

_reactDom2.default.render(_react2.default.createElement(
  _reactRedux.Provider,
  { store: store },
  _react2.default.createElement(
    _styledComponents.ThemeProvider,
    { theme: _theme2.default },
    _react2.default.createElement(_AppContainer2.default, null)
  )
), document.getElementById('app'));