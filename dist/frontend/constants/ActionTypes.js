'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
const RETRIEVE_LAPTOPS = exports.RETRIEVE_LAPTOPS = 'RETRIEVE_LAPTOPS';
const RETRIEVE_LAPTOPS_FAILED = exports.RETRIEVE_LAPTOPS_FAILED = 'RETRIEVE_LAPTOPS_FAILED';
const SELECT_LAPTOP = exports.SELECT_LAPTOP = 'SELECT_LAPTOP';
const RETRIEVE_CATEGORIES = exports.RETRIEVE_CATEGORIES = 'RETRIEVE_CATEGORIES';
const RETRIEVE_CATEGORIES_FAILED = exports.RETRIEVE_CATEGORIES_FAILED = 'RETRIEVE_CATEGORIES_FAILED';
const ADD_COMPONENT = exports.ADD_COMPONENT = 'ADD_COMPONENT';
const GET_TOTAL_PRICE = exports.GET_TOTAL_PRICE = 'GET_TOTAL_PRICE';