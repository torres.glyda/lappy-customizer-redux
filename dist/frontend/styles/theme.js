'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
const theme = {
  primary: 'tomato',
  secondary: 'dimgray',
  accent1: 'white',
  accent2: 'rgb(60, 60, 60)',
  accent3: 'gainsboro'
};

exports.default = theme;