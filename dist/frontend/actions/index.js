'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addComponent = exports.retrieveComponentCategories = exports.retrieveLaptops = exports.selectLaptop = undefined;

var _client = require('../client');

var _client2 = _interopRequireDefault(_client);

var _ActionTypes = require('../constants/ActionTypes');

var types = _interopRequireWildcard(_ActionTypes);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const selectLaptop = exports.selectLaptop = laptop => dispatch => {
  dispatch({
    type: types.SELECT_LAPTOP,
    selectedLaptop: laptop
  });
};

const retrieveLaptops = exports.retrieveLaptops = () => (() => {
  var _ref = _asyncToGenerator(function* (dispatch) {
    try {
      const laptops = yield _client2.default.service('api/laptops').find();
      dispatch({
        type: types.RETRIEVE_LAPTOPS,
        laptops
      });
    } catch (error) {
      dispatch({
        type: types.RETRIEVE_LAPTOPS_FAILED,
        error
      });
    }
  });

  return function (_x) {
    return _ref.apply(this, arguments);
  };
})();

const retrieveComponentCategories = exports.retrieveComponentCategories = () => (() => {
  var _ref2 = _asyncToGenerator(function* (dispatch) {
    try {
      const componentCategories = yield _client2.default.service('api/componentCategories').find();
      dispatch({
        type: types.RETRIEVE_CATEGORIES,
        componentCategories
      });
    } catch (error) {
      dispatch({
        type: types.RETRIEVE_CATEGORIES_FAILED,
        error
      });
    }
  });

  return function (_x2) {
    return _ref2.apply(this, arguments);
  };
})();

const addComponent = exports.addComponent = component => (dispatch, getState) => {
  dispatch({
    type: types.ADD_COMPONENT,
    component
  });
};