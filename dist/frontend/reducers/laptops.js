'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getTotalPrice = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _redux = require('redux');

var _ActionTypes = require('../constants/ActionTypes');

var types = _interopRequireWildcard(_ActionTypes);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

const defaultState = {
  laptops: [],
  selectedLaptop: {},
  componentCategories: [],
  selectedComponents: [],
  totalPrice: 0
};

const getTotalPrice = exports.getTotalPrice = state => {
  return state.laptopStore.selectedLaptop.price + state.laptopStore.selectedComponents.reduce((sum, component) => sum + component.price, 0);
};

const getSelectedComponents = (state, action) => {
  const multipleSelectedCategories = ['accessory', 'bag'];
  let index = -1;
  if (multipleSelectedCategories.includes(action.component.category.name)) {
    index = state.selectedComponents.findIndex(selectedComponent => selectedComponent._id === action.component._id);
    if (index > -1) return [...state.selectedComponents.slice(0, index), ...state.selectedComponents.slice(index + 1)];
  } else {
    index = state.selectedComponents.findIndex(selectedComponent => selectedComponent.category._id === action.component.category._id);
    return [...state.selectedComponents.slice(0, index), action.component, ...state.selectedComponents.slice(index + 1)];
  }
  return [...state.selectedComponents, action.component];
};

exports.default = (state = defaultState, action = {}) => {
  switch (action.type) {
    case types.RETRIEVE_LAPTOPS:
      return _extends({}, state, {
        laptops: action.laptops
      });
    case types.SELECT_LAPTOP:
      return _extends({}, state, {
        selectedLaptop: action.selectedLaptop
      });
    case types.ADD_COMPONENT:
      return _extends({}, state, {
        selectedComponents: getSelectedComponents(state, action)
      });
    case types.RETRIEVE_CATEGORIES:
      return _extends({}, state, {
        componentCategories: action.componentCategories
      });
    case types.RETRIEVE_LAPTOPS_FAILED:
    case types.RETRIEVE_CATEGORIES_FAILED:
      console.log('ERROR: ', action.error);
      return state;
    default:
      return state;
  }
};