'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _redux = require('redux');

var _laptops = require('./laptops');

var _laptops2 = _interopRequireDefault(_laptops);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const reducers = {
  laptopStore: _laptops2.default
};

const appReducer = (0, _redux.combineReducers)(reducers);

exports.default = appReducer;