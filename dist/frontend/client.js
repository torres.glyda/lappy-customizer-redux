'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _client = require('feathers/client');

var _client2 = _interopRequireDefault(_client);

var _feathersSocketio = require('feathers-socketio');

var _feathersSocketio2 = _interopRequireDefault(_feathersSocketio);

var _socket = require('socket.io-client');

var _socket2 = _interopRequireDefault(_socket);

var _feathersHooks = require('feathers-hooks');

var _feathersHooks2 = _interopRequireDefault(_feathersHooks);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const protocol = location.href.includes('localhost') ? 'http' : 'https';
let url = `${protocol}://${location.host}`;

const client = (0, _client2.default)();
const socketConnection = (0, _socket2.default)(url);

client.configure((0, _feathersSocketio2.default)(socketConnection)).configure((0, _feathersHooks2.default)());

window.app = client;

exports.default = client;