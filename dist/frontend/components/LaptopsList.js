'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouterDom = require('react-router-dom');

var _components = require('../styles/components');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const displayLaptop = selectLaptop => laptop => _react2.default.createElement(
  _components.LaptopSelectionContainer,
  { key: laptop._id },
  _react2.default.createElement(
    _reactRouterDom.Link,
    { className: 'link', to: `/${laptop.name.replace(/"|\s/g, '').toLowerCase()}`, onClick: () => selectLaptop(laptop) },
    _react2.default.createElement(
      'p',
      null,
      laptop.name
    ),
    _react2.default.createElement(
      'p',
      null,
      'PHP ',
      laptop.price.toFixed(2)
    ),
    _react2.default.createElement('img', { src: laptop.imgSrc })
  )
);

const LaptopsList = ({ laptopStore, selectLaptop }) => _react2.default.createElement(
  'div',
  null,
  _react2.default.createElement(
    'p',
    null,
    'Select a laptop:'
  ),
  laptopStore.laptops.map(displayLaptop(selectLaptop))
);

exports.default = LaptopsList;