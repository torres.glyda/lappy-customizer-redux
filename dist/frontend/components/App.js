'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _LaptopsContainer = require('../containers/LaptopsContainer');

var _LaptopsContainer2 = _interopRequireDefault(_LaptopsContainer);

var _LaptopDesignContainer = require('../containers/LaptopDesignContainer');

var _LaptopDesignContainer2 = _interopRequireDefault(_LaptopDesignContainer);

var _Home = require('./Home');

var _Home2 = _interopRequireDefault(_Home);

var _reactRouterDom = require('react-router-dom');

var _components = require('../styles/components');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const App = ({ laptopStore }) => _react2.default.createElement(
  _reactRouterDom.HashRouter,
  null,
  _react2.default.createElement(
    'div',
    null,
    _react2.default.createElement(
      _components.Header,
      null,
      _react2.default.createElement('hr', null),
      _react2.default.createElement(
        'h2',
        null,
        ' ',
        _react2.default.createElement(
          _reactRouterDom.Link,
          { to: '/', className: 'link' },
          'Lappy Customizer'
        )
      ),
      _react2.default.createElement('hr', null)
    ),
    _react2.default.createElement(_reactRouterDom.Route, { path: '/', component: _Home2.default }),
    _react2.default.createElement(_reactRouterDom.Route, { path: '/laptops', component: _LaptopsContainer2.default }),
    laptopStore.laptops.map(laptop => _react2.default.createElement(_reactRouterDom.Route, {
      key: laptop._id,
      path: `/${laptop.name.replace(/"|\s/g, '').toLowerCase()}`,
      component: () => _react2.default.createElement(_LaptopDesignContainer2.default, { laptop: laptop })
    }))
  )
);

exports.default = App;