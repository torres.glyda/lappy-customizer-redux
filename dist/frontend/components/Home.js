'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouterDom = require('react-router-dom');

var _components = require('../styles/components');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const Home = () => _react2.default.createElement(
	_reactRouterDom.Link,
	{ to: '/laptops' },
	_react2.default.createElement(
		_components.Button,
		null,
		'Laptops'
	)
);

exports.default = Home;