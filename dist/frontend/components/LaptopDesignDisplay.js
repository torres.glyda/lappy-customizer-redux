'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouterDom = require('react-router-dom');

var _ComponentsList = require('./ComponentsList');

var _ComponentsList2 = _interopRequireDefault(_ComponentsList);

var _components = require('../styles/components');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const LaptopDesignDisplay = ({ laptopStore, addComponent, laptop, selectLaptop }) => _react2.default.createElement(
	'div',
	null,
	_react2.default.createElement(
		_components.PriceDiv,
		null,
		'PHP',
		laptopStore.totalPrice.toFixed(2)
	),
	_react2.default.createElement(
		_components.LaptopDesignHeader,
		null,
		_react2.default.createElement(
			'span',
			null,
			'DESIGN YOUR'
		),
		_react2.default.createElement(
			'p',
			null,
			laptop.name
		)
	),
	laptopStore.componentCategories.map(category => _react2.default.createElement(_ComponentsList2.default, _extends({ key: category._id, components: laptop.components }, { laptopStore, category, addComponent })))
);

exports.default = LaptopDesignDisplay;