'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _components = require('../styles/components');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const multipleSelectedCategories = ['accessory', 'bag'];

const displayList = (laptopStore, category, addComponent) => component => component.category._id === category._id ? _react2.default.createElement(
	'div',
	{ key: component._id },
	_react2.default.createElement('input', {
		id: component._id,
		type: multipleSelectedCategories.includes(category.name) ? 'checkbox' : 'radio',
		name: category.name,
		onChange: () => addComponent(component),
		checked: laptopStore.selectedComponents.some(selectedComponent => selectedComponent._id === component._id)
	}),
	_react2.default.createElement(
		'label',
		{ htmlFor: component._id },
		_react2.default.createElement(
			'span',
			null,
			component.name
		),
		' -',
		_react2.default.createElement(
			'span',
			null,
			' PHP ',
			component.price
		)
	)
) : '';

const ComponentsList = ({ laptopStore, category, components, addComponent }) => _react2.default.createElement(
	_components.CategoriesContainer,
	null,
	_react2.default.createElement(
		'p',
		null,
		category.name
	),
	components.map(displayList(laptopStore, category, addComponent))
);

exports.default = ComponentsList;