'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _actions = require('../actions');

var _LaptopsList = require('../components/LaptopsList');

var _LaptopsList2 = _interopRequireDefault(_LaptopsList);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const mapStateToProps = state => {
  return {
    laptopStore: state.laptopStore
  };
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, { selectLaptop: _actions.selectLaptop })(_LaptopsList2.default);