'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _laptops = require('../reducers/laptops');

var _actions = require('../actions');

var _LaptopDesignDisplay = require('../components/LaptopDesignDisplay');

var _LaptopDesignDisplay2 = _interopRequireDefault(_LaptopDesignDisplay);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class LaptopDesignContainer extends _react2.default.Component {

  componentWillMount() {
    if (Object.keys(this.props.laptopStore.selectedLaptop).length === 0) {
      this.props.selectLaptop(this.props.laptop);
    }
  }

  render() {
    return _react2.default.createElement(_LaptopDesignDisplay2.default, this.props);
  }
}

const mapStateToProps = state => {
  return {
    laptopStore: _extends({}, state.laptopStore, { totalPrice: (0, _laptops.getTotalPrice)(state) })
  };
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, { addComponent: _actions.addComponent, selectLaptop: _actions.selectLaptop })(LaptopDesignContainer);