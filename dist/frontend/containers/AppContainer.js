'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _actions = require('../actions');

var _App = require('../components/App');

var _App2 = _interopRequireDefault(_App);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class AppContainer extends _react2.default.Component {

  componentDidMount() {
    this.props.retrieveLaptops();
    this.props.retrieveComponentCategories();
  }

  render() {
    return _react2.default.createElement(_App2.default, this.props);
  }

}

const mapStateToProps = state => ({
  laptopStore: state.laptopStore
});

exports.default = (0, _reactRedux.connect)(mapStateToProps, { retrieveLaptops: _actions.retrieveLaptops, retrieveComponentCategories: _actions.retrieveComponentCategories })(AppContainer);