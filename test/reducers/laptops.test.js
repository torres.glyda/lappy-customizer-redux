import reducer from '../../src/frontend/reducers/laptops'
import * as types from '../../src/frontend/constants/ActionTypes'

describe('laptops reducer', () => {
  let laptops = [];

  beforeAll(() => {
    laptops = [{
      _id: '596ba86c6cad6a1aa8277fd5',
      name: 'Lemur 14"',
      price: 649.00,
      desc: 'Perfect for those on the go, Lemur is easy to carry from meeting to meeting or across campus.',
    }]
  })

  it('should return the default state', () => {
    expect(reducer()).toEqual({
      laptops: [],
      selectedLaptop: {},
      componentCategories: [],
      selectedComponents: [],
      totalPrice: 0,
    })
  })

  it('should handle RETRIEVE_LAPTOPS', () => {
    expect(
      reducer([], {
        type: types.RETRIEVE_LAPTOPS,
        laptops
      })
    ).toEqual({
      laptops
    })
  })

  it('should handle SELECT_LAPTOP', () => {
    const selectedLaptop = laptops[0];
    expect(
      reducer([], {
        type: types.SELECT_LAPTOP,
        selectedLaptop
      })
    ).toEqual({
      selectedLaptop
    })
  })

  it('should handle ADD_COMPONENT', () => {
    const component = {
      _id: 1,
      name: 'Messenger Bag',
      category: {
        _id: 2,
        name: 'bag',
      },
      price: 1200.00,
    }
    expect(
      reducer({ selectedComponents: [] }, {
        type: types.ADD_COMPONENT,
        component,
      })
    ).toEqual({
      selectedComponents: [component],
    })
  })
})