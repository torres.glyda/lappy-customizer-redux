import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from '../../src/frontend/actions'
import * as types from '../../src/frontend/constants/ActionTypes'
import * as client from '../../src/frontend/client'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('actions', () => {

  let store;

  beforeAll(() => {
    const laptops = [{
      _id: '596ba86c6cad6a1aa8277fd5',
      name: 'Lemur 14"',
      price: 649.00,
      desc: 'Perfect for those on the go, Lemur is easy to carry from meeting to meeting or across campus.',
    }]
    client.default.service = jest.fn().mockReturnValue({
      find: () => laptops,
    });
  })

  beforeEach(() => {
    store = mockStore({ laptops: [], selectedLaptop: {} })
  })

  it('should create an action to retrieve laptops', () => {
    const laptops = [{
      _id: '596ba86c6cad6a1aa8277fd5',
      name: 'Lemur 14"',
      price: 649.00,
      desc: 'Perfect for those on the go, Lemur is easy to carry from meeting to meeting or across campus.',
    }]
    const expectedAction = [{
      type: types.RETRIEVE_LAPTOPS,
      laptops: laptops,
    }]

    return store.dispatch(actions.retrieveLaptops())
      .then(() => {
        expect(store.getActions()).toEqual(expectedAction)
      })
  })

  it('should create an action to select laptop', () => {
    const laptop = {
      _id: '596ba86c6cad6a1aa8277fd5',
      name: 'Lemur 14"',
      price: 649.00,
      desc: 'Perfect for those on the go, Lemur is easy to carry from meeting to meeting or across campus.',
    }
    const expectedAction = [{
      type: types.SELECT_LAPTOP,
      selectedLaptop: laptop,
    }]

    store.dispatch(actions.selectLaptop(laptop))
    expect(store.getActions()).toEqual(expectedAction)
  })

  it('should create an action to add a component', () => {
    const component = {
      _id: 1,
      name: 'Messenger Bag',
      category: {
        _id: 2,
        name: 'bag',
      },
      price: 1200.00,
    }
    const expectedAction = [{
      type: types.ADD_COMPONENT,
      component,
    }]

    store.dispatch(actions.addComponent(component))
    expect(store.getActions()).toEqual(expectedAction)
  })
})