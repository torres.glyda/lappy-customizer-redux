import setupServer from './setupServer';

const port = process.env.PORT || 3000;

const startServer = async() => {
  const app = await setupServer();
  app.listen(port, () => {
    console.log(`App is running at http://localhost:${app.get('port')}`);
  });
}

startServer();