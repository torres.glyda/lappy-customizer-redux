import setupService from './setupService';

import populate from '../../hooks/populate';

const setupLaptopService = (db) => {
  const before = {};
  const after = {
    find: [
      populate('api/components', '_id', 'componentIds', 'components'),
    ],
  };
  return function() {
    const laptopService = setupService(this, db, 'laptops');
    laptopService
      .before(before)
      .after(after);
  }
}

export default setupLaptopService;