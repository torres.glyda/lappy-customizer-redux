import setupService from './setupService';

import populate from '../../hooks/populate';

const setupComponentService = (db) => {
  const before = {};
  const after = {
    find: [
      populate('api/componentCategories', '_id', 'categoryId', 'category', true),
    ],
  };
  return function() {
    const componentService = setupService(this, db, 'components');
    componentService
      .before(before)
      .after(after);
  }
}

export default setupComponentService;