import laptop from './laptop';
import component from './component';
import componentCategory from './componentCategory';

const services = (db) => {
  return function() {
    const app = this;
    app
      .configure(laptop(db))
      .configure(component(db))
      .configure(componentCategory(db))
  }
}

export default services;