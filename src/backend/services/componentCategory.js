import setupService from './setupService';

const setupComponentCategoryService = (db) => {
  const before = {};
  const after = {};
  return function() {
    const componentCategoryService = setupService(this, db, 'componentCategories');
    componentCategoryService
      .before(before)
      .after(after);
  }
}

export default setupComponentCategoryService;