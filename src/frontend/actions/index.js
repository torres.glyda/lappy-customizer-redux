import client from '../client'
import * as types from '../constants/ActionTypes'

export const selectLaptop = laptop => dispatch => {
  dispatch({
    type: types.SELECT_LAPTOP,
    selectedLaptop: laptop,
  })
}

export const retrieveLaptops = () => async dispatch => {
  try {
    const laptops = await client.service('api/laptops').find()
    dispatch({
      type: types.RETRIEVE_LAPTOPS,
      laptops,
    })
  } catch (error) {
    dispatch({
      type: types.RETRIEVE_LAPTOPS_FAILED,
      error,
    })
  }
}

export const retrieveComponentCategories = () => async dispatch => {
  try {
    const componentCategories = await client.service('api/componentCategories').find()
    dispatch({
      type: types.RETRIEVE_CATEGORIES,
      componentCategories,
    })
  } catch (error) {
    dispatch({
      type: types.RETRIEVE_CATEGORIES_FAILED,
      error,
    })
  }
}

export const addComponent = component => (dispatch, getState) => {
  dispatch({
    type: types.ADD_COMPONENT,
    component,
  })
}