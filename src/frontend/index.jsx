import React from 'react'
import ReactDOM from 'react-dom'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

import { ThemeProvider } from 'styled-components';
import theme from './styles/theme';

import appReducer from './reducers';
import AppContainer from './containers/AppContainer'

const middleware = [thunk]
const store = createStore(appReducer, applyMiddleware(...middleware))

ReactDOM.render(
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <AppContainer />
    </ThemeProvider>
  </Provider>,
  document.getElementById('app')
);
