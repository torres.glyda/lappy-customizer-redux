const theme = {
  primary: 'tomato',
  secondary: 'dimgray',
  accent1: 'white',
  accent2: 'rgb(60, 60, 60)',
  accent3: 'gainsboro',
};

export default theme;