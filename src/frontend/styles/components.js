import styled, { injectGlobal } from 'styled-components';

const center = 'margin: 0 auto';

injectGlobal `
	@import url('https://fonts.googleapis.com/css?family=Open+Sans');

	body {
		font-family: 'Open Sans', sans-serif;
		text-align: center;
	}

	.link {
		text-decoration: none;
		color: white;
	}
`;

export const Header = styled.header `
	${center};
	background: rgb(60, 60, 60);
`;

export const LaptopSelectionContainer = styled.div `
	margin-right: 10px;
	padding-bottom: 10px;
	display: inline-block;
	width: 300px;
  background: ${({theme}) => theme.accent1};
	box-shadow: 1px 1px 6px 0 rgba(0, 0, 0, 0.3);
	border: 1px solid white;
	p {
		color: ${({theme}) => theme.accent2};
		font-size: 25px;
		letter-spacing: 1px;
	};
	img {
		display: block;
		margin:auto;
		width: auto;
		max-height: 150px;
	};
	&:hover {
		border-color: #FE9393;
	};
`

export const PriceDiv = styled.div `
	position: fixed;
	top: 30px;
	left: 10px;
	bottom: 10px;
	width: 100px;
	height: 23px;
	overflow-y: auto;
	color: ${({theme}) => theme.accent1};
	background: ${({theme}) => theme.accent2};
	border-radius: 2px;
`;

export const LaptopDesignHeader = styled.div `
	span{
		font-size: 20px;
		font-weight: bold;
	};
	p {
		font-size: 25px;
		text-transform: uppercase;
		letter-spacing: 1px;
	};
`;

export const Button = styled.button `
	font-size: 15px;
	margin: 12px;
	padding: 4px 15px;
	cursor: pointer;
	color: ${({primary, theme}) => primary ? theme.accent1 : theme.primary};
	border: 2px solid ${({theme}) => theme.primary};
	border-radius: 3px;
	background-color: ${({primary, theme}) => primary ?  theme.primary : theme.accent1};
	&:hover {
		background-color: ${({primary, theme}) => primary ?  theme.secondary : theme.accent1};
		color: ${({primary, theme}) => primary ? theme.accent1 : theme.secondary};
		border-color: ${({primary, theme}) => theme.secondary};
	};
`;

export const CategoriesContainer = styled.div `
	p {
		font-size: 20px;
		letter-spacing: 2px;
		text-transform: uppercase;
	};
	input {
		display: none;
	};
	label {
		display: inline-block;
		width: 500px;
		background-color: ${({theme}) => theme.accent3};
		padding: 5px 10px;
		font-size: 14px;
		cursor: pointer;
	};
	input {
		&:checked+label, &:hover+label {
			color: ${({theme}) => theme.accent1};
			background-color: ${({theme}) => theme.primary};
		};
		+label {
			border: 1px solid black;
		};
	};
`;