import { combineReducers } from 'redux'
import laptops from './laptops'

const reducers = {
  laptopStore: laptops
}

const appReducer = combineReducers(reducers)

export default appReducer