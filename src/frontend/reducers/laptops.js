import { combineReducers } from 'redux'
import * as types from '../constants/ActionTypes'

const defaultState = {
  laptops: [],
  selectedLaptop: {},
  componentCategories: [],
  selectedComponents: [],
  totalPrice: 0,
}

export const getTotalPrice = (state) => {
  return state.laptopStore.selectedLaptop.price + state.laptopStore.selectedComponents.reduce((sum, component) => sum + component.price, 0)
}

const getSelectedComponents = (state, action) => {
  const multipleSelectedCategories = ['accessory', 'bag']
  let index = -1
  if (multipleSelectedCategories.includes(action.component.category.name)) {
    index = state.selectedComponents.findIndex(selectedComponent => selectedComponent._id === action.component._id)
    if (index > -1) return [...state.selectedComponents.slice(0, index), ...state.selectedComponents.slice(index + 1)]
  } else {
    index = state.selectedComponents.findIndex(selectedComponent => selectedComponent.category._id === action.component.category._id)
    return [...state.selectedComponents.slice(0, index), action.component, ...state.selectedComponents.slice(index + 1)]
  }
  return [...state.selectedComponents, action.component]
}

export default (state = defaultState, action = {}) => {
  switch (action.type) {
    case types.RETRIEVE_LAPTOPS:
      return {
        ...state,
        laptops: action.laptops,
      }
    case types.SELECT_LAPTOP:
      return {
        ...state,
        selectedLaptop: action.selectedLaptop,
      }
    case types.ADD_COMPONENT:
      return {
        ...state,
        selectedComponents: getSelectedComponents(state, action),
      }
    case types.RETRIEVE_CATEGORIES:
      return {
        ...state,
        componentCategories: action.componentCategories,
      }
    case types.RETRIEVE_LAPTOPS_FAILED:
    case types.RETRIEVE_CATEGORIES_FAILED:
      console.log('ERROR: ', action.error)
      return state
    default:
      return state;
  }
}