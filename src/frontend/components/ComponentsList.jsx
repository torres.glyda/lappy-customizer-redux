import React from 'react'

import { CategoriesContainer } from '../styles/components'

const multipleSelectedCategories = ['accessory', 'bag']

const displayList = (laptopStore, category, addComponent) => component =>
	component.category._id === category._id
		? <div key={component._id}>
			<input
				id={component._id}
				type={multipleSelectedCategories.includes(category.name) ? 'checkbox' : 'radio'}
				name={category.name}
				onChange={() => addComponent(component)}
				checked={laptopStore.selectedComponents.some(selectedComponent => selectedComponent._id === component._id)}
			/>
			<label htmlFor={component._id}>
				<span >{component.name}</span> -
        <span> PHP {component.price}</span>
			</label>
		</div>
		: ''

const ComponentsList = ({ laptopStore, category, components, addComponent }) => (
	<CategoriesContainer>
		<p>{category.name}</p>
		{components.map(displayList(laptopStore, category, addComponent))}
	</CategoriesContainer>
)

export default ComponentsList