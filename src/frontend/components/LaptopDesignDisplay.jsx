import React from 'react'
import { Link } from 'react-router-dom'
import ComponentsList from './ComponentsList'

import { PriceDiv, LaptopDesignHeader } from '../styles/components'

const LaptopDesignDisplay = (({ laptopStore, addComponent, laptop, selectLaptop }) => (
	<div>
		<PriceDiv>PHP{laptopStore.totalPrice.toFixed(2)}</PriceDiv>
		<LaptopDesignHeader>
			<span>DESIGN YOUR</span>
			<p>{laptop.name}</p>
		</LaptopDesignHeader>
		{laptopStore.componentCategories.map(category =>
			<ComponentsList key={category._id} components={laptop.components} {...{laptopStore, category, addComponent}} />
		)}
	</div>
));

export default LaptopDesignDisplay;