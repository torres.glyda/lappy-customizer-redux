import React from 'react'
import LaptopsContainer from '../containers/LaptopsContainer'
import LaptopDesignContainer from '../containers/LaptopDesignContainer'
import Home from './Home'
import { HashRouter as Router, Route, Link } from 'react-router-dom';

import { Header } from '../styles/components'

const App = ({ laptopStore }) => (
  <Router>
    <div>
      <Header>
        <hr />
        <h2> <Link to='/' className='link'>Lappy Customizer</Link></h2>
        <hr />
      </Header>
      <Route path='/' component={Home} />
      <Route path='/laptops' component={LaptopsContainer} />
      {laptopStore.laptops.map(laptop =>
        <Route
          key={laptop._id}
          path={`/${laptop.name.replace(/"|\s/g, '').toLowerCase()}`}
          component={() => <LaptopDesignContainer laptop={laptop} />}
        />
      )}
    </div>
  </Router>
)

export default App