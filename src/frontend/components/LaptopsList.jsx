import React from 'react';
import { Link } from 'react-router-dom';

import { LaptopSelectionContainer } from '../styles/components'

const displayLaptop = (selectLaptop) => (laptop) => (
  <LaptopSelectionContainer key={laptop._id} >
    <Link className='link' to={`/${laptop.name.replace(/"|\s/g, '').toLowerCase()}`} onClick={() => selectLaptop(laptop)}>
      <p>{laptop.name}</p>
      <p>PHP {laptop.price.toFixed(2)}</p>
      <img src={laptop.imgSrc} />
    </Link>
  </LaptopSelectionContainer>
)

const LaptopsList = (({ laptopStore, selectLaptop }) => (
  <div>
    <p>Select a laptop:</p>
    {laptopStore.laptops.map(displayLaptop(selectLaptop))}
  </div>
))

export default LaptopsList;