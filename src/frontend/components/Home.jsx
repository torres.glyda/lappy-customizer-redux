import React from 'react'
import { Link } from 'react-router-dom'

import { Button } from '../styles/components'

const Home = () => (
	<Link to='/laptops'>
		<Button>Laptops</Button >
	</Link>
)

export default Home