import React from 'react'
import { connect } from 'react-redux'
import { getTotalPrice } from '../reducers/laptops'
import { addComponent, selectLaptop } from '../actions'
import LaptopDesignDisplay from '../components/LaptopDesignDisplay'

class LaptopDesignContainer extends React.Component {

  componentWillMount() {
    if (Object.keys(this.props.laptopStore.selectedLaptop).length === 0) {
      this.props.selectLaptop(this.props.laptop)
    }
  }

  render() {
    return <LaptopDesignDisplay {...this.props } />
  }
}

const mapStateToProps = state => {
  return {
    laptopStore: { ...state.laptopStore, totalPrice: getTotalPrice(state) }
  }
}

export default connect(mapStateToProps, { addComponent, selectLaptop })(LaptopDesignContainer)