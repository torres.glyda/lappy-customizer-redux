import React from 'react'
import { connect } from 'react-redux'
import { retrieveLaptops, retrieveComponentCategories } from '../actions'
import App from '../components/App'

class AppContainer extends React.Component {

  componentDidMount() {
    this.props.retrieveLaptops()
    this.props.retrieveComponentCategories()
  }

  render() {
    return <App {...this.props} />
  }

}

const mapStateToProps = state => ({
  laptopStore: state.laptopStore,
})

export default connect(mapStateToProps, { retrieveLaptops, retrieveComponentCategories })(AppContainer)