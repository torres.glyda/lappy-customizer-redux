import React from 'react'
import { connect } from 'react-redux'
import { selectLaptop } from '../actions'
import LaptopsList from '../components/LaptopsList'

const mapStateToProps = state => {
  return {
    laptopStore: state.laptopStore,
  }
}

export default connect(mapStateToProps, { selectLaptop })(LaptopsList)