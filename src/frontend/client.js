import feathers from 'feathers/client';
import feathersSocketio from 'feathers-socketio';
import socketioClient from 'socket.io-client';
import hooks from 'feathers-hooks';

const protocol = location.href.includes('localhost') ? 'http' : 'https';
let url = `${protocol}://${location.host}`;

const client = feathers();
const socketConnection = socketioClient(url);

client
  .configure(feathersSocketio(socketConnection))
  .configure(hooks());

window.app = client;

export default client;