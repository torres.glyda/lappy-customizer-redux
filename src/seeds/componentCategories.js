import setupServer from '../backend/setupServer';
import { ObjectId } from 'mongodb';

const seed = async() => {
  const app = await setupServer();

  const componentCategory = [{
      _id: ObjectId('59c5031546278b0fccb1ad17'),
      name: 'os'
    },
    {
      _id: ObjectId('59c5031546278b0fccb1ad18'),
      name: 'processor'
    },
    {
      _id: ObjectId('59c5031546278b0fccb1ad19'),
      name: 'accessory'
    },
    {
      _id: ObjectId('59c5031546278b0fccb1ad1a'),
      name: 'bag'
    },
  ];

  const componentCategoryService = await app.service('api/componentCategories');
  await componentCategoryService.remove(null);
  return componentCategoryService.create(componentCategory);

};

export default seed;