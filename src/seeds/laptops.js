import setupServer from '../backend/setupServer';
import { ObjectId } from 'mongodb';

const seed = async() => {
  const app = await setupServer();

  const laptops = [{
      _id: ObjectId('596ba86c6cad6a1aa8277fd5'),
      name: 'Lemur 14"',
      price: 649.00,
      desc: 'Perfect for those on the go, Lemur is easy to carry from meeting to meeting or across campus.',
      componentIds: [
        ObjectId('59c5047aad576a0434e407be'),
        ObjectId('59c5047aad576a0434e407bf'),
        ObjectId('59c5047aad576a0434e407c2'),
        ObjectId('59c5047aad576a0434e407c3'),
        ObjectId('59c5047aad576a0434e407c4'),
        ObjectId('59c5047aad576a0434e407c5'),
        ObjectId('59c5047aad576a0434e407c6'),
        ObjectId('59c5047aad576a0434e407c7'),
        ObjectId('59c5047aad576a0434e407c8'),
      ],
      imgSrc: 'images/laptops/lemur-14.jpg',
    },
    {
      _id: ObjectId('596ba86c6cad6a1aa8277fd6'),
      name: 'Gazelle 15"',
      price: 719.00,
      desc: 'Gazelle’s definitive lines sports a high quality, sleek finish. Its points and curves draws the attention of those more bold, making the Gazelle the perfect maker or hacker companion.',
      componentIds: [
        ObjectId('59c5047aad576a0434e407bf'),
        ObjectId('59c5047aad576a0434e407c0'),
        ObjectId('59c5047aad576a0434e407c2'),
        ObjectId('59c5047aad576a0434e407c3'),
        ObjectId('59c5047aad576a0434e407c4'),
        ObjectId('59c5047aad576a0434e407c5'),
        ObjectId('59c5047aad576a0434e407c6'),
        ObjectId('59c5047aad576a0434e407c7'),
        ObjectId('59c5047aad576a0434e407c8'),
      ],
      imgSrc: 'images/laptops/gazelle-15.jpg',
    },
    {
      _id: ObjectId('596ba86c6cad6a1aa8277fd7'),
      name: 'Kudu 17"',
      price: 939.00,
      desc: 'Kudu’s high quality finish is sharp and clean, appealing to those who prefer sheer and pure simplicity. Its straight edges and lines keep your machine looking new, and keeps you feeling professional.',
      componentIds: [
        ObjectId('59c5047aad576a0434e407c0'),
        ObjectId('59c5047aad576a0434e407c1'),
        ObjectId('59c5047aad576a0434e407c2'),
        ObjectId('59c5047aad576a0434e407c3'),
        ObjectId('59c5047aad576a0434e407c4'),
        ObjectId('59c5047aad576a0434e407c5'),
        ObjectId('59c5047aad576a0434e407c6'),
        ObjectId('59c5047aad576a0434e407c7'),
        ObjectId('59c5047aad576a0434e407c8'),
      ],
      imgSrc: 'images/laptops/kudu-17.jpg',
    },
  ];

  const laptopsService = await app.service('api/laptops');
  await laptopsService.remove(null);
  return laptopsService.create(laptops);

};

export default seed;