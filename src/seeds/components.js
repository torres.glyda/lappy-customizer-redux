import setupServer from '../backend/setupServer';
import { ObjectId } from 'mongodb';

const seed = async() => {
  const app = await setupServer();

  const components = [{
      _id: ObjectId('59c5047aad576a0434e407be'),
      name: 'Ubuntu 14.0.21 LTS (64-bit)',
      categoryId: ObjectId('59c5031546278b0fccb1ad17'),
      price: 0.00,
    }, {
      _id: ObjectId('59c5047aad576a0434e407bf'),
      name: 'Ubuntu 15.32.2 LTS (64-bit)',
      categoryId: ObjectId('59c5031546278b0fccb1ad17'),
      price: 0.00,
    }, {
      _id: ObjectId('59c5047aad576a0434e407c0'),
      name: 'Ubuntu 16.04.2 LTS (64-bit)',
      categoryId: ObjectId('59c5031546278b0fccb1ad17'),
      price: 0.00,
    },
    {
      _id: ObjectId('59c5047aad576a0434e407c1'),
      name: 'Ubuntu 17.04 (64-bit)',
      categoryId: ObjectId('59c5031546278b0fccb1ad17'),
      price: 800.00,
    },
    {
      _id: ObjectId('59c5047aad576a0434e407c2'),
      name: 'i3-7100u (3 MB Cache - 2 Cores - 4 Threads)',
      categoryId: ObjectId('59c5031546278b0fccb1ad18'),
      price: 0.00,
    },
    {
      _id: ObjectId('59c5047aad576a0434e407c3'),
      name: 'i7-7500U (2.7 up to 3.5 - 4 MB Cache - 2 Cores - 4 Threads)',
      categoryId: ObjectId('59c5031546278b0fccb1ad18'),
      price: 3500.00,
    },
    {
      _id: ObjectId('59c5047aad576a0434e407c4'),
      name: 'Messenger Bag',
      categoryId: ObjectId('59c5031546278b0fccb1ad1a'),
      price: 850.00,
    },
    {
      _id: ObjectId('59c5047aad576a0434e407c5'),
      name: 'Laptop Backpack',
      categoryId: ObjectId('59c5031546278b0fccb1ad1a'),
      price: 900.00,
    },
    {
      _id: ObjectId('59c5047aad576a0434e407c6'),
      name: 'Extra AC Adapter',
      categoryId: ObjectId('59c5031546278b0fccb1ad19'),
      price: 450.00,
    },
    {
      _id: ObjectId('59c5047aad576a0434e407c7'),
      name: 'Extra Battery',
      categoryId: ObjectId('59c5031546278b0fccb1ad19'),
      price: 550.00,
    },
    {
      _id: ObjectId('59c5047aad576a0434e407c8'),
      name: 'Extrernal USB DVD-RW Drive',
      categoryId: ObjectId('59c5031546278b0fccb1ad19'),
      price: 1300.00,
    },
  ];

  const componentsService = await app.service('api/components');
  await componentsService.remove(null);
  return componentsService.create(components);

};

export default seed;